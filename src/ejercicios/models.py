from django.db import models


class IngresoEjercicios(models.Model):


    nombre = models.CharField(max_length=100, blank=False, null=False)
    coordenadas_correcta = models.TextField()
    codigo_ejercicio = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100, blank=False, null=False)
    imagen = models.CharField(max_length=100, blank=False, null=False,default='ingrese imagen')
    fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)


    def __str__(self):
        return self.nombre




# Create your models here.
