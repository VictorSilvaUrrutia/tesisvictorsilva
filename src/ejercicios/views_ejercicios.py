from django.shortcuts import render
from .forms import RegForm
from .models import IngresoEjercicios

def inicio3(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data

        nombre2 = form_data.get("nombre")
        descripcion2 = form_data.get("descripcion")
        imagen2 = form_data.get("imagen")
        coordenadas_correcta2=form_data.get("coordenadas_correcta")


        objeto = IngresoEjercicios.objects.create(nombre=nombre2, descripcion=descripcion2, imagen=imagen2,coordenadas_correcta= coordenadas_correcta2 )

       # if form.is_valid():
         #   instancia = form.save(commit=False)  # Esta línea impide la creación de objetos
         #   instance.save()  # CON ESTA SENTENCIA SE CREAN LOS OBJETOS EN LA BASE DE DATOS
          #  print(instance)

    contexto = {
        "el_formulario3": form,
    }

    return render(request, "inicio3.html", contexto)

