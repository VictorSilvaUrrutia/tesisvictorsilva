from django.contrib import admin
from .models import IngresoEjercicios
from .forms import RegModelForm2

class AdminIngresoEjercicios(admin.ModelAdmin):

    list_display = ["codigo_ejercicio", "nombre", "descripcion","imagen", "coordenadas_correcta"]
    model = RegModelForm2
    list_filter = ["codigo_ejercicio"]
    search_fields = ["nombre", "descripcion"]
   # list_editable = ["hardware"]


#class Meta:
      #model = IngresoEjercicios

# Register your models here.

admin.site.register(IngresoEjercicios, AdminIngresoEjercicios)

