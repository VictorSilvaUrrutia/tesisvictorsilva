"""tesis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from especialistas import views
from pacientes import views_pacientes
from ejercicios import views_ejercicios
from planes_ejercicios import views_planes
from ejecucion import views_ejecucion
from api.views import ListarIngresoEjerciciosViewSets
from api.views import ListarIngresoEjerciciosViewSets
from api.views import ListarIngresoPlanesEjerciciosViewSets
from api.views import ListarIngresoPacientesViewSets
from api.views import ActualizarIngresoEjecucionViewSets
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^inicio/$', views.inicio, name='inicio'),
    url(r'^inicio2/$', views_pacientes.inicio2, name='inicio2'),
    url(r'^inicio3/$', views_ejercicios.inicio3, name='inicio3'),
    url(r'^inicio4/$', views_planes.inicio4, name='inicio4'),
    url(r'^inicio5/$', views_ejecucion.inicio5, name='inicio5'),
    url(r'^api/ejercicioslist/$', ListarIngresoEjerciciosViewSets.as_view(), name='inicio7'),
    url(r'^api/ejercicioscrear/$', ListarIngresoEjerciciosViewSets.as_view(), name='inicio8'),
    url(r'^api/planesejercicios/$', ListarIngresoPlanesEjerciciosViewSets.as_view(), name='inicio8'),
    url(r'^api/pacienteslistar/$', ListarIngresoPacientesViewSets.as_view(), name='inicio9'),
    url(r'^api/ejecucionActualizar/(?P<pk>[0-9]+)$', ActualizarIngresoEjecucionViewSets.as_view(), name='inicio10'),

]
urlpatterns = format_suffix_patterns(urlpatterns)