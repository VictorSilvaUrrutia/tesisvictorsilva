from django.shortcuts import render
from .forms import RegForm
from .models import IngresoPacientes
from django.urls import reverse_lazy
from utils.views import UtilListView, UtilDeleteView, UtilCreateView, UtilUpdateView
from . import forms, models, tables
from django_filters.views import FilterView


class ListView(FilterView, UtilListView):
    template_name = 'paciente/list.html'
    table_class = tables.IngresoPacientesTable
    paginate_by = 20
    model_class = models.IngresoPacientes
    required_permissions = ('pacientes.view_pacientes',)
    change_permission = 'pacientes.change_pacientes'
    delete_permission = 'pacientes.delete_pacientes'






def inicio2(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        rut2 = form_data.get("rut")
        nombres2 = form_data.get("nombres")
        apellido_paterno2 = form_data.get("apellido_paterno")
        apellido_materno2 = form_data.get("apellido_materno")
        fecha_nacimiento2 = form_data.get("fecha_nacimiento")
        email2 = form_data.get("email")
        direccion2 = form_data.get("direccion")
        num_calle2 = form_data.get("num_calle")
        telefono2 = form_data.get("telefono")
        estado2 = form_data.get("estado")
        objeto2 = IngresoPacientes.objects.create(rut=rut2, nombres=nombres2, apellido_paterno=apellido_paterno2, apellido_materno=apellido_materno2, fecha_nacimiento=fecha_nacimiento2, email=email2, direccion=direccion2, num_calle=num_calle2, telefono=telefono2, estado=estado2)

       # if form.is_valid():
         #   instancia = form.save(commit=False)  # Esta línea impide la creación de objetos
         #   instance.save()  # CON ESTA SENTENCIA SE CREAN LOS OBJETOS EN LA BASE DE DATOS
          #  print(instance)

    contexto = {
        "el_formulario2": form,
    }

    return render(request, "inicio2.html", contexto)

