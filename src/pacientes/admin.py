from django.contrib import admin
from .models import IngresoPacientes
from .forms import RegModelForm2

#from .models import Estado

#class AdminEstado(admin.ModelAdmin):

    #list_display = ['nombre']
    #search_fields = ['nombre']

    #class Meta:
        #model = Estado





class AdminIngresoPacientes(admin.ModelAdmin):
    list_display = ["rut", "nombres", "apellido_paterno", "apellido_materno" , "fecha_nacimiento", "email", "direccion", "num_calle", "telefono", "especialista","estado"]
    model = RegModelForm2
   # list_filter = ["rut"]
    search_fields = ["nombres", "apellido_paterno", "estado"]
   # list_editable = ["hardware"]




#class Meta:
       #model = IngresoPacientes

# Register your models here.





admin.site.register(IngresoPacientes, AdminIngresoPacientes)

