from django.urls import reverse_lazy
from utils.tables import UtilTable
from .models import IngresoPacientes

class IngresoPacientesTable(UtilTable):
    class Meta:
        model = IngresoPacientes
        template = 'django_tables2/bootstrap-responsive.html'
        sequence = (
            'id',
            'rut',
            'nombres',
            'apellido_paterno',
            'apellido_materno',
            'fecha_nacimiento',
            'fecha_ingreso',
            'email',
            'direccion',
            'num_calle',
            'telefono',
            'estado',
        )

    def reverse_update(self, record):
        return reverse_lazy('pacientes:pacientes_update', kwargs={'pk': record.pk})

    def reverse_delete(self, record):
        return reverse_lazy('pacientes:pacientes_delete', kwargs={'pk': record.pk})