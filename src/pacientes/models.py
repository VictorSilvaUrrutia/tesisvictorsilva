from django.db import models
from especialistas.models import IngresoKine
from model_utils import Choices


#class Estado(models.Model):


    #nombre = models.CharField(max_length=20)

    #def __str__(self):
        #return self.nombre





class IngresoPacientes(models.Model):

    opciones = Choices('activo','inactivo')




    rut = models.CharField(max_length=10, blank=False, null=False)
    nombres = models.CharField(max_length=100, blank=False, null=False)
    apellido_paterno = models.CharField(max_length=100, blank=False, null=False)
    apellido_materno = models.CharField(max_length=100, blank=False, null=False, default='S/A')
    fecha_nacimiento = models.DateField(auto_now_add=False, auto_now=False)
    fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)
    email = models.EmailField()
    direccion = models.CharField(max_length=100, null=True)
    num_calle = models.PositiveIntegerField()
    telefono = models.PositiveIntegerField()
    estado = models.CharField(choices=opciones,default=opciones.activo,max_length=20)
    especialista = models.ForeignKey(IngresoKine,default=0)


    def __str__(self):
        return self.nombres



# Create your models here.
