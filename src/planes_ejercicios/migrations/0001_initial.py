# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-27 12:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='IngresoPlanesEjercicios',
            fields=[
                ('codigo_plan', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.CharField(max_length=100)),
                ('logro_plan', models.TextField()),
            ],
        ),
    ]
