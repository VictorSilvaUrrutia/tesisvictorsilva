from django.shortcuts import render
from .forms import RegForm
from .models import IngresoPlanesEjercicios

def inicio4(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
         form_data = form.cleaned_data
         nombre2 = form_data.get("nombre")
         descripcion2 = form_data.get("descripcion")


         objeto4 = IngresoPlanesEjercicios.objects.create(nombre=nombre2, descripcion=descripcion2)

       # if form.is_valid():
            #instancia = form.save(commit=False)  # Esta línea impide la creación de objetos
          #  instance.save()  # CON ESTA SENTENCIA SE CREAN LOS OBJETOS EN LA BASE DE DATOS
           # print(instance)

    contexto = {
        "el_formulario4": form,
    }

    return render(request, "inicio4.html",contexto )

