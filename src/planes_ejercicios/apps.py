from django.apps import AppConfig


class PlanesEjerciciosConfig(AppConfig):
    name = 'planes_ejercicios'
