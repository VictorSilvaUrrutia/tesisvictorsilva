from django.contrib import admin
from .models import IngresoPlanesEjercicios
from .forms import RegModelForm4





class AdminIngresoPlanesEjercicios(admin.ModelAdmin):

    list_display = ["codigo_plan", "nombre",'paciente' ,"descripcion","ejercicios"]
    model = RegModelForm4
    list_filter = ["codigo_plan"]
    search_fields = ["nombre", "descripcion"]
   # list_editable = ["hardware"]


#lass Meta:
      #model = IngresoPlanesEjercicios

# Register your models here.

admin.site.register(IngresoPlanesEjercicios, AdminIngresoPlanesEjercicios)

