from django.db import models
from pacientes.models import IngresoPacientes
from ejercicios.models import IngresoEjercicios

#from model_utils import Choices


class IngresoPlanesEjercicios(models.Model):

   # opciones = Choices(IngresoEjercicios.objects.all)

    codigo_plan = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=False, null=False)
    paciente = models.ForeignKey(IngresoPacientes, default=0)
    ejercicios = models.ForeignKey(IngresoEjercicios,default=0)

    descripcion = models.CharField(max_length=100, blank=False, null=False)
    #logro_plan = models.DecimalField(decimal_places=2,max_digits=5)



    def __str__(self):
        return self.nombre




# Create your models here.
