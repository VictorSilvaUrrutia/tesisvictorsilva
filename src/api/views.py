from django.shortcuts import render

from rest_framework import viewsets, generics
from rest_framework.generics import ListAPIView
from .serializers import IngresoEjerciciosSerializers
from .serializers import IngresoPlanesEjerciciosSerializers
from .serializers import IngresoPacientesSerializers
from .serializers import IngresoEjecucionSerializers
from ejercicios.models import IngresoEjercicios
from planes_ejercicios.models import IngresoPlanesEjercicios
from pacientes.models import IngresoPacientes
from ejecucion.models import IngresoEjecucion




class ListarIngresoEjerciciosViewSets(generics.ListAPIView):
    queryset = IngresoEjercicios.objects.all()
    serializer_class = IngresoEjerciciosSerializers
    authentication_classes = []
    permission_classes = []

class ListarIngresoEjerciciosViewSets(generics.ListAPIView):
    queryset = IngresoEjercicios.objects.all()
    serializer_class = IngresoEjerciciosSerializers
    authentication_classes = []
    permission_classes = []



class ListarIngresoPlanesEjerciciosViewSets(generics.ListAPIView):
    queryset = IngresoPlanesEjercicios.objects.all()
    serializer_class = IngresoPlanesEjerciciosSerializers
    authentication_classes = []
    permission_classes = []

class ListarIngresoPacientesViewSets(generics.ListAPIView):
    queryset = IngresoPacientes.objects.all()
    serializer_class = IngresoPacientesSerializers
    authentication_classes = []
    permission_classes = []

class ActualizarIngresoEjecucionViewSets(generics.UpdateAPIView):
    queryset = IngresoEjecucion.objects.all()
    serializer_class = IngresoEjecucionSerializers
    authentication_classes = []
    permission_classes = []