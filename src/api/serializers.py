from rest_framework import serializers
from ejercicios.models import IngresoEjercicios
from planes_ejercicios.models import IngresoPlanesEjercicios
from pacientes.models import IngresoPacientes
from ejecucion.models import IngresoEjecucion



#Cambiar despues a plan de ejercicios
class IngresoEjerciciosSerializers(serializers.ModelSerializer):
    class Meta:
        model = IngresoEjercicios
        fields = ['codigo_ejercicio',  'nombre','descripcion','imagen',"coordenadas_correcta"]




class IngresoPlanesEjerciciosSerializers(serializers.ModelSerializer):
    class Meta:
        model = IngresoPlanesEjercicios
        fields = ['ejercicios', 'descripcion']


class IngresoPacientesSerializers(serializers.ModelSerializer):
    class Meta:
        model = IngresoPacientes
        fields = ['nombres', 'apellido_paterno','apellido_paterno', 'email', 'direccion','num_calle','telefono','estado','especialista']



class IngresoEjecucionSerializers(serializers.ModelSerializer):
    class Meta:
        model = IngresoEjecucion
        fields = ['coordenadas', 'ejercicio','paciente','plan_ejercicio']

