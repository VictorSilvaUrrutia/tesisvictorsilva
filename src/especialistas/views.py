from django.shortcuts import render
from .forms import RegForm
from .models import IngresoKine

def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        rut2 = form_data.get("rut")
        nombres2 = form_data.get("nombres")
        apellido_paterno2 = form_data.get("apellido_paterno")
        apellido_materno2 = form_data.get("apellido_materno")
        direccio_inst2 = form_data.get("direccion_inst")
        num_calle2 = form_data.get("num_calle")
        email2 = form_data.get("email")
        telefono2 = form_data.get("telefono")
        objeto = IngresoKine.objects.create(rut=rut2, nombres=nombres2, apellido_paterno=apellido_paterno2, apellido_materno=apellido_materno2, direccion_inst=direccio_inst2, num_calle=num_calle2, email=email2, telefono=telefono2)

       # if form.is_valid():
         #   instancia = form.save(commit=False)  # Esta línea impide la creación de objetos
         #   instance.save()  # CON ESTA SENTENCIA SE CREAN LOS OBJETOS EN LA BASE DE DATOS
          #  print(instance)

    contexto = {
        "el_formulario": form,
    }

    return render(request, "inicio.html", contexto)

