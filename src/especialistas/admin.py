from django.contrib import admin
from .models import IngresoKine
from .forms import RegModelForm

class AdminIngresoKine(admin.ModelAdmin):

    list_display = ["rut", "nombres", "apellido_paterno","apellido_materno", "direccion_inst", "email", "telefono"]
    model = RegModelForm
    list_filter = ["rut"]
    search_fields = ["nombres", "rut", "apellido_paterno", "apellido_materno"]
   # list_editable = ["hardware"]


#class Meta:
      # model = IngresoKine

# Register your models here.

admin.site.register(IngresoKine, AdminIngresoKine)

