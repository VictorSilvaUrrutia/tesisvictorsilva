from django import forms
from .models import IngresoKine
from model_utils import Choices
from itertools import cycle




class RegForm(forms.Form):
    # registro_id = forms.AutoField(primary_key=True)
    rut = forms.CharField(max_length=10)
    nombres = forms.CharField(max_length=100)
    apellido_paterno = forms.CharField(max_length=100)
    apellido_materno = forms.CharField(max_length=100)
    direccion_inst = forms.CharField(max_length=100)
    num_calle = forms.IntegerField()
    email = forms.EmailField()
    telefono = forms.IntegerField()

    def clean_nombres(self):
        nombres = self.cleaned_data.get("nombres")
        if nombres:  # Validamos que el contenido del campo nombre no venga vacío
            if len(nombres) <= 1:  # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter, NOMBRE NO VALIDO!")
            return nombres
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")


class RegModelForm(forms.ModelForm):
    class Meta:
        modelo = IngresoKine
        campos = ["nombres","nombres", "apellido_paterno", "apellido_materno", "direccion_inst", "num_calle", "email", "telefono"]

    def clean_nombres(self):
        nombres = self.cleaned_data.get("nombres")
        if nombres:  # Validamos que el contenido del campo nombre no venga vacío
            if len(nombres) <= 1:  # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter")
            return nombres
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")



    def clean_rut(self):
        rut = self.cleaned_data['rut']
        rut = rut.upper();
        rut = rut.replace("-", "")
        rut = rut.replace(".", "")
        aux = rut[:-1]
        dv = rut[-1:]

        revertido = map(int, reversed(str(aux)))
        factors = cycle(range(2, 8))
        s = sum(d * f for d, f in zip(revertido, factors))
        res = (-s) % 11

        if str(res) == dv:
            return rut
        elif dv == "K" and res == 10:
            return rut
        else:
            raise forms.ValidationError('RUT incorrecto')