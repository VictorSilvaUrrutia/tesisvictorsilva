from django.db import models

class IngresoKine(models.Model):

    rut = models.CharField(max_length=10, blank=False, null=False)
    nombres = models.CharField(max_length=100, blank=False, null=False)
    apellido_paterno = models.CharField(max_length=100, blank=False, null=False)
    apellido_materno = models.CharField(max_length=100, blank=False, null=False, default='S/A')
    direccion_inst = models.CharField(max_length=100, null=True)
    num_calle = models.PositiveIntegerField()
    email = models.EmailField()
    telefono = models.CharField(verbose_name="telefono", max_length=9)
    fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)


    def __str__(self):
        return self.nombres




# Create your models here.
