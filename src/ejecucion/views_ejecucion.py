from django.shortcuts import render
from .forms import RegForm
from .models import IngresoEjecucion

def inicio5(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data


        coordenadas2 = form_data.get("coordenadas")


        objeto = IngresoEjecucion.objects.create( coordenadas=coordenadas2)

       # if form.is_valid():
         #   instancia = form.save(commit=False)  # Esta línea impide la creación de objetos
         #   instance.save()  # CON ESTA SENTENCIA SE CREAN LOS OBJETOS EN LA BASE DE DATOS
          #  print(instance)

    contexto = {
        "el_formulario5": form,
    }

    return render(request, "inicio5.html", contexto)

