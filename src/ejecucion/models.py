from django.db import models
from ejercicios.models import IngresoEjercicios
from pacientes.models import IngresoPacientes
from planes_ejercicios.models import IngresoPlanesEjercicios



class IngresoEjecucion(models.Model):

    codigo_ejecucion = models.AutoField(primary_key=True)
    coordenadas = models.TextField()
    fecha_ejecucion = models.DateTimeField(auto_now_add=False, auto_now=True)
    ejercicio = models.ForeignKey(IngresoEjercicios,default=0)
    paciente = models.ForeignKey(IngresoPacientes,default=0)
    plan_ejercicio = models.ForeignKey(IngresoPlanesEjercicios,default=0)


    def __str__(self):
        return self.coordenadas




# Create your models here.
