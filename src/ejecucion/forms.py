from django import forms
from .models import IngresoEjecucion



class RegForm(forms.Form):
    # registro_id = forms.AutoField(primary_key=True)

    coordenadas = forms.TextInput()



    def clean_coordenadas(self):
        coordendas = self.cleaned_data.get("coordendas")
        if  coordendas:  # Validamos que el contenido del campo nombre no venga vacío
            if len( coordendas) <= 1:  # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter, NOMBRE NO VALIDO!")
            return  coordendas
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")


class RegModelForm5(forms.ModelForm):
    class Meta:
        modelo = IngresoEjecucion
        campos = ["coordenadas"]

    def clean_coordenadas(self):
        coordenadas= self.cleaned_data.get("coordendas")
        if  coordenadas:  # Validamos que el contenido del campo nombre no venga vacío
            if len( coordenadas) <= 1:  # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter")
            return coordenadas
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")
