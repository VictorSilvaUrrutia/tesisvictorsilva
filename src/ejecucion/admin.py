from django.contrib import admin

from .forms import RegModelForm5
from .models import IngresoEjecucion


class AdminIngresoEjecucion(admin.ModelAdmin):

    list_display = ["codigo_ejecucion", "coordenadas","fecha_ejecucion"]
    model = RegModelForm5
    list_filter = ["codigo_ejecucion"]
    search_fields = ["coordenadas", "fecha_ejecucion"]
   # list_editable = ["hardware"]


#class Meta:
       #model = IngresoEjecucion

# Register your models here.

admin.site.register(IngresoEjecucion, AdminIngresoEjecucion)
admin.site.site_header = 'Panel de Adminstracion de Especialistas y Pacientes de Kineaccion'

